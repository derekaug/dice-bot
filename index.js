`use strict`;

require('dotenv').config();

process.title = process.argv[2];

let RtmClient = require(`@slack/client`).RtmClient;
let CLIENT_EVENTS = require(`@slack/client`).CLIENT_EVENTS;
let RTM_EVENTS = require(`@slack/client`).RTM_EVENTS;
let _ = require(`underscore`);
let GamblersDie = require('gamblers-dice');

let bot = {
    /**
     * slack client instance
     */
    rtm: null,

    gamblers_dice: [],

    /**
     * configuration variables for the slack channel
     */
    config: {
        token: process.env.SLACK_BOT_TOKEN || '',
        auto_reconnect: true,
        main_channel: process.env.SLACK_MAIN_CHANNEL || 'dice-test'
    },

    /**
     *
     */
    overrides: {
        // unicode 𝟶𝟷𝟸𝟹𝟺𝟻𝟼𝟽𝟾𝟿 ¯\_(ツ)_/¯
        '\uD835\uDFF8': 2,
        '\uD835\uDFF9': 3,
        '\uD835\uDFFA': 4,
        '\uD835\uDFFB': 5,
        '\uD835\uDFFC': 6,
        '\uD835\uDFFD': 7,
        '\uD835\uDFFE': 8,
        '\uD835\uDFFF': 9,
        '\uD835\uDFF7\uD835\uDFF6': 10,
        '\uD835\uDFF7\uD835\uDFF7': 11,
        '\uD835\uDFF7\uD835\uDFF8': 12
    },

    /**
     *
     */
    numbers: {
        // numerals
        '2': 2,
        '3': 3,
        '4': 4,
        '5': 5,
        '6': 6,
        '7': 7,
        '8': 8,
        '9': 9,
        '10': 10,
        '11': 11,
        '12': 12,
        // english
        'two': 2,
        'three': 3,
        'four': 4,
        'five': 5,
        'six': 6,
        'seven': 7,
        'eight': 8,
        'nine': 9,
        'ten': 10,
        'eleven': 11,
        'twelve': 12,
        // spanish
        'dos': 2,
        'tres': 3,
        'cuatro': 4,
        'cinco': 5,
        'seis': 6,
        'siete': 7,
        'ocho': 8,
        'nueve': 9,
        'diez': 10,
        'once': 11,
        'doce': 12,
        // german
        'zwei': 2,
        'drei': 3,
        'vier': 4,
        'f\u00FCnf': 5,
        'sechs': 6,
        'sieben': 7,
        'acht': 8,
        'neun': 9,
        'zehn': 10,
        'elf': 11,
        'zw\u00F6lf': 12
    },

    /**
     * various messaging the bot uses
     */
    messages: {
        fail: [
            'No match.',
            'That sucks.',
            'Maybe next time.',
            'Fuck.',
            'Wow, such fail.',
            'Failure',
            'Is Craig cool?',
            'This generic blow off was kind of a let down.',
            'Johnny broke me.',
            'I\'m on vacation, leave me alone',
            'I AM A POOR BOY TOO!',
            'Sometimes you think you do, sometimes you think you don\'t'
        ],
        doubles: [
            'Doubles, rolling again.',
            'Double your pleasure, double your fun!',
            'Fill my eyes with that double vision.',
            'My double vision gets the best of me.',
            'Wow, such dice, much doubles!',
            'Man, you are an emotional roller coaster.'
        ],
        success: [
            'Matched, pay up fuckers!',
            'Seems luck was on your side.',
            'Bitch better have my money!',
            'Pay me what you owe me!',
            'Yeah buddy!',
            'Fuck yeah, bitch!',
            'Left Flank... Fire!',
            'I\'m going with the A+!',
            'Your eyes, hazy with the mist jealousy leaves behind on a warm day.',
            `Show me what you got! :schwifty:`
        ],
        connected: [
            `I'm back, bitches! :troll:`,
            `The server rebooted, my brain feels like mush... :confused:`,
            `UPGRADE!!! :nu:`
        ]
    },

    regex: {},

    /**
     * initialize the bot
     */
    init: function () {
        this.numbers = _.extend(this.numbers, this.overrides);
        this.regex.roll = new RegExp('^.*(if[\\s]+i[\\s]+roll|on)[\\s]+an?[\\s]+(' + Object.keys(this.numbers).join('|') + ')[^\\d].*$', 'i');
        this.rtm = new RtmClient(this.config.token, {
            autoReconnect: this.config.auto_reconnect
        });

        // events
        this.rtm.on(CLIENT_EVENTS.RTM.AUTHENTICATED, _.bind(this.onAuthenticated, this));
        this.rtm.on(CLIENT_EVENTS.RTM.RTM_CONNECTION_OPENED, _.bind(this.onConnectionOpened, this));

        // initialize gamblers dice
        this.gamblers_dice = [
            new GamblersDie(6),
            new GamblersDie(6)
        ];

        // start the rtm client
        this.rtm.start();
    },

    /**
     *
     *
     * @param start_data
     */
    onAuthenticated: function (start_data) {

    },

    /**
     *
     */
    onConnectionOpened: function () {
        this.sendMessage(this.randomMessage(this.messages.connected), this.getMainChannel());
        this.rtm.on(RTM_EVENTS.MESSAGE, _.bind(this.onMessage, this));
    },

    /**
     *
     * @returns {Object}
     */
    getMainChannel: function () {
        let rvalue = this.rtm.dataStore.getChannelByName(this.config.main_channel);
        if (!rvalue) {
            this.rtm.dataStore.getGroupByName(this.config.main_channel)
        }
        return rvalue;
    },

    /**
     * when a user sends a message to the channel, check it to see if it's a roll
     *
     * @param message
     */
    onMessage: function (message) {
        let channel = this.rtm.dataStore.getChannelGroupOrDMById(message.channel);
        if (this.isValidMessage(message) && this.isValidChannel(channel)) {
            // if it is a roll
            let roll_match = message.text.match(this.regex.roll);
            if (roll_match && roll_match.length && roll_match[2]) {
                this.roll(roll_match[2], channel);

            }
        }
    },

    /**
     * determines if dice-bot is allowed to dice roll in this channel
     *
     * @param channel
     * @returns {*|boolean}
     */
    isValidChannel: function (channel) {
        let isDiceGame = channel && channel.name === 'dice-game',
            isDiceTest = channel && channel.name === 'dice-test';

        return isDiceGame || isDiceTest;
    },

    /**
     * is a valid message for dice-bot to determine if he should roll
     *
     * @param message
     * @returns {boolean}
     */
    isValidMessage: function (message) {
        return message.type === 'message' && typeof message.subtype === 'undefined';
    },

    /**
     * roll the dice [needs refactored]
     *
     * @param number_to_match
     * @param channel
     * @returns {*}
     */
    roll: function (number_to_match, channel) {
        number_to_match = this.coerceNumber(number_to_match);

        let reroll = false,
            dice1 = this.rollDie(0),
            dice2 = this.rollDie(1),
            total = dice1 + dice2,
            is_doubles = dice1 === dice2,
            is_match = number_to_match === total,
            message = ['*' + dice1 + '* + *' + dice2 + '* = *_' + total + '_*'];

        if (is_match) {
            // match
            message.push('! _' + this.randomMessage(this.messages.success) + '_ :sunglasses:');
        }
        else {
            if (is_doubles) {
                // doubles, roll again
                reroll = true;
                message.push('. _' + this.randomMessage(this.messages.doubles) + '_ :smirk:');
            }
            else {
                // no match
                message.push('. _' + this.randomMessage(this.messages.fail) + '_ :pensive:');
            }
        }

        this.sendMessage(message.join(''), channel);
        return reroll ? this.roll(number_to_match, channel) : is_match;
    },

    /**
     * roll a specific die, 0-indexed
     *
     * @param die
     * @returns {*}
     */
    rollDie: function (die) {
        // die = die % this.gamblers_dice.length;
        // return this.gamblers_dice[die].roll();
        return Math.floor((Math.random() * 6) + 1);
    },

    /**
     * get random message
     *
     * @param messages
     * @returns {*}
     */
    randomMessage: function (messages) {
        return messages[Math.floor((Math.random() * messages.length))];
    },

    /**
     * automatically hit the number that is rolled
     *
     * @param number_to_match
     * @param channel
     * @returns {boolean}
     */
    hit: function (number_to_match, channel) {
        number_to_match = this.coerceNumber(number_to_match);

        let max = number_to_match > 6 ? 6 : number_to_match - 1,
            dice1 = Math.floor((Math.random() * max) + 1),
            dice2 = number_to_match - dice1,
            total = dice1 + dice2,
            message = [],
            is_match = true;

        message.push('*' + dice1 + '* + *' + dice2 + '* = *_' + total + '_*');
        message.push('! _' + this.randomMessage(this.messages.success) + '_ :sunglasses:');

        this.sendMessage(message.join(''), channel);

        return is_match;
    },

    /**
     * coerce
     *
     * @param number
     * @returns {number}
     */
    coerceNumber: function (number) {
        let rvalue = 0,
            parsed = parseInt(number, 10);

        if (this.numbers[number]) {
            rvalue = this.numbers[number];
        }
        else if (!(isNaN(parsed))) {
            rvalue = parsed;
        }

        return rvalue;
    },

    /**
     * send a message to a channel
     *
     * @param message
     * @param channel
     */
    sendMessage: function (message, channel) {
        if (channel) {
            this.rtm.sendMessage(message, channel.id);
        }
    }
};

// start him up
bot.init();