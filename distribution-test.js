let GamblersDie = require('gamblers-dice');

const gamblers_dice = [
    new GamblersDie(6),
    new GamblersDie(6)
];

let gamblers_dist = [
    null, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, null
];
let normal_dist = [
    null, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, null
];

for (let i = 0; i < 10000; i++) {

    const gamblers_1 = gamblers_dice[0].roll();
    const gamblers_2 = gamblers_dice[0].roll();

    const normal_1 = getRandomIntInclusive(1, 6);
    const normal_2 = getRandomIntInclusive(1, 6);

    const gamblers_res = gamblers_1 + gamblers_2;
    const normal_res = normal_1 + normal_2;

    gamblers_dist[gamblers_res]++;
    normal_dist[normal_res]++
}

console.log(gamblers_dist);
console.log(normal_dist)



function getRandomIntInclusive(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min; //The maximum is inclusive and the minimum is inclusive
}
